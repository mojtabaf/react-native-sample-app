/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import SearchMovie from './components/search/SearchMovie'
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    NavigatorIOS
} from 'react-native';
import WatchList from "./components/watch/WatchList";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#111111',
    }
});

export default class MobileR extends Component {
    constructor(props){
        super(props);
    }
    _handleNavigationRequest(){
        this.refs.nav.push({
            title: 'Watch List',
            component: WatchList,
        });
    }
    render() {
        return (
            <NavigatorIOS
                ref="nav"
                style={styles.container}
                initialRoute={{
                title: 'Search',
                component: SearchMovie,
                rightButtonTitle: 'List',
                onRightButtonPress: () => this._handleNavigationRequest(),
            }} />
        );
    }
};


AppRegistry.registerComponent('MobileR', () => MobileR);
