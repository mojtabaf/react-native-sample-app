/**
 * Created by arz on 2017-03-12.
 */
import React from 'react';
import {Text, View, Image} from 'react-native';
import MovieDetail from "../search/MovieDetail";
const WatchListRow = ({navigator, movie}) => {
    const onDetail = () =>{
        console.log('navigate');
        // navigator.push({
        //     title: movie.Title,
        //     component: MovieDetail,
        //     passProps: {movie: movie}
        // })
    }
    return(
        <View style={styles.container}>
            <View style={styles.firstContainer}>
                <View style={styles.imgContainer}>
                    <Image style={styles.img}
                        source={{uri: movie.Poster}}
                        />
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.headerTextStyle}>{movie.Title}</Text>
                    <Text>{movie.watched ? "Watched": "Unwatched"}</Text>
                </View>
            </View>
             <View style={styles.secondContainer}>
                <View style={styles.textContainer}>
                    <Text>{movie.Year}</Text>
                    <Text>{movie.like ? "Recommend": "Not Recommend"}</Text>

                </View>
            </View>
        </View>

    );
};
const styles ={
    container:{
        borderRadius: 2,
        borderWidth: 1,
        borderBottomWidth: 1,
        borderColor: "#ddd",
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {width:0, height: 2},
        shadowOpacity: 0.3,
        shadowRadius: 4,
        marginTop: 10,
        marginRight: 10,
        marginLeft: 10,
        elevation: 1,
        padding: 5,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    firstContainer:{
        justifyContent: 'flex-start',
        flexDirection: 'row'
    },
    secondContainer:{
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    textContainer:{
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    headerTextStyle:{
        fontSize:18
    },
    imgContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },

    img:{
        width:50,
        height:50
    }
}
export default WatchListRow;
