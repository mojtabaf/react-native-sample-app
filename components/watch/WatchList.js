/**
 * Created by arz on 2017-03-11.
 */
import * as React from "react";
import WatchListRow from './WatchListRow';
import {
    Text, View, Image, StyleSheet, Header
} from 'react-native';
export default class WatchList extends React.Component{
    constructor(props, context){
        super(props, context);
        this.state = {movies : [{
            id: 1,
            Title: 'Jackie',
            Year: '2016',
            Poster:'https://images-na.ssl-images-amazon.com/images/M/MV5BNzY2NzI4OTE5MF5BMl5BanBnXkFtZTcwMjMyNDY4Mw@@._V1_SX300.jpg',
            watched: false,
            like: false
         },
         {
             id: 2,
             Title: 'Nocturnal Animals',
             Year: '2016',
             Poster:'https://images-na.ssl-images-amazon.com/images/M/MV5BMzg4MjYzNjk5N15BMl5BanBnXkFtZTgwODgwODI3MDI@._V1_SX300.jpg',
             watched: false,
             like: false
         }],
        list:[]};
        this.renderAlbums = this.renderAlbums.bind(this);
    }
    onDetail(){
        console.log('navigate');
    }
    renderAlbums(){
        return this.state.movies.map(movie => <WatchListRow key={movie.id} navigator={this.props.navigator} movie={movie} onPress={this.onDetail.bind(this)}/>)
    }
    render(){
        return(
            <View style={{marginTop: 60}}>
                {this.renderAlbums()}

            </View>


        );

    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop:80
    }
});
