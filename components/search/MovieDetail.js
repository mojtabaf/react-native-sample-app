/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import * as React from "react";
import {
    View, Text, Image, StyleSheet, ScrollView, TouchableHighlight, Button
} from 'react-native';
import IMDBView from "./IMDBView";

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#e2e0ff',
        marginTop: 60,
        padding: 20
    },
    title: {
        fontSize: 24,
        color: '#111',
        alignSelf: 'center',
        marginTop: 50,
        marginBottom: 20
    },
    card: {
        width: 300,
        height: 440,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: "#111",
        padding: 20,
        color: '#e2e0ff',
        backgroundColor: '#e2e0ff',
        shadowColor: '#000',
        shadowOffset: {width:0, height: 2},
        shadowOpacity: 0.3,
        shadowRadius: 8
    },
    toolbar: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#e2e0ff'
    },
    firstButtonText: {
        padding: 8,
        color: '#111'
    },
    secondButtonText: {
        padding: 8,
        color: '#111'
    }
});

export default class MovieDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {showStory: false};
        this.openIMDB = this.openIMDB.bind(this);
        this.onShowPlot = this.onShowPlot.bind(this);
    }

    openIMDB() {
        this.props.navigator.push({
            title: 'IMDB',
            component: IMDBView,
            passProps: {imdbId: this.props.movie.imdbID}
        })
    }

    onShowPlot() {
        this.setState({showStory: !this.state.showStory})
    }

    render() {
        let story = (
            this.state.showStory ? <View styel={{backgroundColor: '#111', justifyContent: 'flex-start', padding: 20, flex: 1}}>
                    <Text style={{marginTop:8, marginBottom: 8, color:'#111'}}>
                        {this.props.movie.Plot}
                    </Text>
                </View> : <View></View>
        )
        return (
            <View style={{ backgroundColor: '#e2e0ff', marginTop: 60, padding: 20, flex: 1}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                        <Text color="#fff">
                            Genre:
                        </Text>
                        <Text color="#fff">
                            {this.props.movie.Genre}
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                        <Text color="#fff">
                            Director:
                        </Text>
                        <Text color="#fff">
                            {this.props.movie.Director}
                        </Text>
                    </View>
                </View>
                <View style={{alignItems:'center'}}>
                    <View style={styles.card}>
                        <Image
                            style={{width: '100%', height: 300}}
                            source={{uri: this.props.movie.Poster}}
                        />
                        <View style={{ backgroundColor: '#111', justifyContent: 'flex-start'}}>
                            <Text style={{marginTop:8, marginBottom: 8, color:'#e2e0ff'}}>
                                Actors:
                            </Text>
                            <Text style={{marginBottom: 8, color:'#e2e0ff'}}>
                                {this.props.movie.Actors}
                            </Text>
                        </View>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#111'}}>
                            <Button
                                onPress={this.openIMDB}
                                title="Add"
                                color="#e2e0ff"
                                accessibilityLabel="Add To My List"
                            />
                            <Button
                                onPress={this.onShowPlot}
                                title={!this.state.showStory ? "Plot": "Hide"}
                                color="#e2e0ff"
                                accessibilityLabel="Show Plot"
                            />
                            <Button
                                onPress={this.openIMDB}
                                title="IMDB"
                                color="#e2e0ff"
                                accessibilityLabel="Go To IMDB Website!"
                            />
                        </View>
                    </View>
                    {story}
                </View>

            </View>

        )
    }
}
