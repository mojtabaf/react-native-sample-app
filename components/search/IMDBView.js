/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React from 'react';
import {
    WebView, StyleSheet, View, ScrollView
} from 'react-native'
const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#e2e0ff',
        flexDirection: 'column'
    }
})
export const IMDBView = ({imdbId}) =>{
    const url = `http://www.imdb.com/title/${imdbId}/`;
    return(
            <View style={styles.container}>
                <WebView url={url} />
            </View>
    );
};
export default IMDBView;
