/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import * as React from "react";
import {
    Text, View, Image, StyleSheet, TextInput, TouchableHighlight, ActivityIndicator
} from 'react-native';
import MovieApi from '../../apis/movieApi';
import MovieDetail from "./MovieDetail";
export default class SearchMovie extends React.Component{
    constructor(props){
        super(props);
        this.state={
            movieName: '',
            year:'',
            isLoading: false,
            error: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.onSearchClick = this.onSearchClick.bind(this);
        this.onYearChange = this.onYearChange.bind(this);
    }
    handleChange(event){
        this.setState({movieName: event.nativeEvent.text});
    }
    onYearChange(event){
        this.setState({year: event.nativeEvent.text});
    }
    onSearchClick(){
        this.setState({isLoading: true});
        MovieApi.getMovie(this.state.movieName, this.state.year).then((res)=>{
            if(res.Response === 'False'){
                this.setState({error: 'Movie not found!', isLoading: false});
            }else{
                this.props.navigator.push({
                    title: res.Title || "select an Option",
                    component: MovieDetail,
                    passProps:{movie: res}
                });
                this.setState({
                    isLoading: false,
                    error: false,
                    movieName: '',
                    year:''
                });
            }
        }).catch((error) => {
            console.log(error);
            this.setState({isLoading: false});});
    }
    render(){
        let showError = (
            this.state.error ? <Text>{this.state.error}</Text> : <View></View>
        );
        return(
            <View style={styles.container}>
                <Text style={styles.title}>Search Movie</Text>
                <TextInput
                    style={styles.searchInput}
                    value={this.state.movieName}
                    onChange={this.handleChange}
                    />
                <TextInput
                    style={styles.searchInput}
                    value={this.state.year}
                    onChange={this.onYearChange}
                />
                <TouchableHighlight
                    style={styles.button}
                    onPress={this.onSearchClick}
                    underlayColor="white">
                    <Text style={styles.buttonText}>Search</Text>
                </TouchableHighlight>
                {showError}
                <ActivityIndicator
                    animating={this.state.isLoading}
                    style={[{height: 80}]}
                    color="#111"
                    size="large"
                />
            </View>

        );

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#e2e0ff',
    },
    title:{
        marginBottom: 20,
        fontSize: 25,
        textAlign: 'center',
        color: '#9b57f9'
    },
    searchInput:{
        height:50,
        padding: 4,
        borderColor: '#e2e0ff',
        borderWidth:1,
        borderRadius: 8,
        color: '#111',
        marginLeft: 20,
        marginRight: 20
    },
    buttonText:{
        fontSize:18,
        color: '#111',
        alignSelf: 'center'
    },
    button:{
        height:45,
        flexDirection : 'row',
        backgroundColor:'white',
        borderColor:'white',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        marginTop:10,
        marginLeft: 20,
        marginRight: 20,
        alignSelf: 'stretch',
        justifyContent: 'center'
    }
});
