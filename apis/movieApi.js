/**
 * Created by arz on 2017-03-11.
 */
const MovieApi = {
    getMovie(name, year){
        return fetch(`http://www.omdbapi.com/?t=${name}&y=${year}`).then((res) => res.json());
    }
}
export default MovieApi;
